/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------------------------------------------------
// Include files
//--------------------------------------------------------------------------------
#include <jni.h>
#include <errno.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>
#include <android/native_window_jni.h>

#include "TeapotRenderer.h"
#include "NDKHelper.h"
#include "GLES/glext.h"

#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>
#include <openxr/openxr.hpp>
#include <openxr/openxr_helpers_opengl.hpp>

#include <inttypes.h>
#include <thread>

//-------------------------------------------------------------------------
// Preprocessor
//-------------------------------------------------------------------------
#define HELPER_CLASS_NAME "com/sample/helper/NDKHelper"  // Class name of helper function
//-------------------------------------------------------------------------
// Shared state for our app.
//-------------------------------------------------------------------------
struct android_app;

class Engine
{
    TeapotRenderer renderer_;

    ndk_helper::GLContext *gl_context_;

    bool initialized_resources_;
    bool has_focus_;

    ndk_helper::DoubletapDetector doubletap_detector_;
    ndk_helper::PinchDetector pinch_detector_;
    ndk_helper::DragDetector drag_detector_;
    ndk_helper::PerfMonitor monitor_;

    ndk_helper::TapCamera tap_camera_;

    android_app *app_;

    const ASensor *accelerometer_sensor_;
    ASensorEventQueue *sensor_event_queue_;

    void ShowUI();

    void TransformPosition(ndk_helper::Vec2 &vec);

    bool session_stopping = false;

public:
    xr::DispatchLoaderDynamic xr_dispatch;

private:
    xr::UniqueDynamicInstance instance;
    xr::SystemId system_id;
    xr::ViewConfigurationType view_config_type = xr::ViewConfigurationType::PrimaryStereo;
    std::vector<xr::ViewConfigurationView> viewconfig_views;
    std::vector<xr::View> views;
    std::vector<xr::CompositionLayerProjectionView> projection_views;

public:
    xr::UniqueDynamicSession session;

private:
    xr::UniqueDynamicSpace local_space;
    xr::SessionState state;
    bool running = false;
    int64_t swapchain_format = 0;
    int64_t depth_swapchain_format = 0;

    std::vector<SwapchainData> swapchains;

    void HandleOpenXREvents();

public:
    static void HandleCmd(struct android_app *app, int32_t cmd);

    static int32_t HandleInput(android_app *app, AInputEvent *event);

    Engine();

    ~Engine();

    void SetState(android_app *app);

    int InitDisplay(android_app *app);

    int InitOpenXRInstance(android_app *app);

    int InitOpenXRSession(android_app *app);

    void LoadResources();

    void UnloadResources();

    void DrawFrame();

    bool RenderFrame(xr::FrameState const& frameState, xr::ViewState const & viewState);

    void TermDisplay();

    void TrimMemory();

    bool IsReady() const;

    void SuspendSensors();

};

//-------------------------------------------------------------------------
// Ctor
//-------------------------------------------------------------------------
Engine::Engine()
    : initialized_resources_(false)
    , has_focus_(false)
    , app_(nullptr)
    , accelerometer_sensor_(NULL)
    , sensor_event_queue_(NULL)
{
    gl_context_ = ndk_helper::GLContext::GetInstance();
}

//-------------------------------------------------------------------------
// Dtor
//-------------------------------------------------------------------------
Engine::~Engine()
{
}

/**
 * Load resources
 */
void Engine::LoadResources()
{
    renderer_.Init();
    renderer_.Bind(&tap_camera_);
}

/**
 * Unload resources
 */
void Engine::UnloadResources()
{
    renderer_.Unload();
}

/**
 * Initialize an EGL context for the current display.
 */
int Engine::InitDisplay(android_app *app)
{
    if (!initialized_resources_) {
        gl_context_->Init(app_->window);
        LoadResources();
        initialized_resources_ = true;
    }
    else if (app->window != gl_context_->GetANativeWindow()) {
        // Re-initialize ANativeWindow.
        // On some devices, ANativeWindow is re-created when the app is resumed
        assert(gl_context_->GetANativeWindow());
        UnloadResources();
        gl_context_->Invalidate();
        app_ = app;
        gl_context_->Init(app->window);
        LoadResources();
        initialized_resources_ = true;
    }
    else {
        // initialize OpenGL ES and EGL
        if (EGL_SUCCESS == gl_context_->Resume(app_->window)) {
            UnloadResources();
            LoadResources();
        }
        else {
            assert(0);
        }
    }
    InitOpenXRInstance(app);
    InitOpenXRSession(app);

    ShowUI();

    // Initialize GL state.
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // Note that screen size might have been changed
    glViewport(0, 0, gl_context_->GetScreenWidth(), gl_context_->GetScreenHeight());
    // renderer_.UpdateViewport();
    //
    //  tap_camera_.SetFlip(1.f, -1.f, -1.f);
    //  tap_camera_.SetPinchTransformFactor(2.f, 2.f, 8.f);

    return 0;
}

int Engine::InitOpenXRInstance(android_app *app)
{
    LOGI("In InitOpenXRInstance");
    PFN_xrInitializeLoaderKHR initializeLoader = nullptr;
    if (XR_SUCCEEDED(xrGetInstanceProcAddr(XR_NULL_HANDLE, "xrInitializeLoaderKHR", (PFN_xrVoidFunction *)(&initializeLoader)))) {
        XrLoaderInitInfoAndroidKHR loaderInitInfoAndroid;
        memset(&loaderInitInfoAndroid, 0, sizeof(loaderInitInfoAndroid));
        loaderInitInfoAndroid.type = XR_TYPE_LOADER_INIT_INFO_ANDROID_KHR;
        loaderInitInfoAndroid.next = NULL;
        loaderInitInfoAndroid.applicationVM = app->activity->vm;
        loaderInitInfoAndroid.applicationContext = app->activity->clazz;
        initializeLoader((const XrLoaderInitInfoBaseHeaderKHR *)&loaderInitInfoAndroid);
    }
    const char *extensions[2] = {XR_KHR_ANDROID_CREATE_INSTANCE_EXTENSION_NAME, XR_KHR_OPENGL_ES_ENABLE_EXTENSION_NAME};
    auto instanceCreateInfoAndroid = xr::InstanceCreateInfoAndroidKHR{app->activity->vm, app->activity->clazz};
    auto instanceCreateInfo = xr::InstanceCreateInfo{
        {}, xr::ApplicationInfo{"OpenXR Sample Teapot", 1, nullptr, 0, xr::Version::current()}, 0, nullptr, 2, extensions};
    instanceCreateInfo.next = &instanceCreateInfoAndroid;
    // We have to provide the dispatch here, because it gets baked into the unique handle.
    instance = xr::createInstanceUnique(instanceCreateInfo, xr_dispatch);
    // Must update our dispatch once we have an instance
    xr_dispatch = xr::DispatchLoaderDynamic::createFullyPopulated(instance.get().get(), ::xrGetInstanceProcAddr);
    system_id = instance->getSystem(xr::SystemGetInfo{xr::FormFactor::HeadMountedDisplay}, xr_dispatch);

    viewconfig_views = instance->enumerateViewConfigurationViewsToVector(system_id, view_config_type, xr_dispatch);

    // Have to do this
    xr::GraphicsRequirementsOpenGLESKHR graphicsRequirements = instance->getOpenGLESGraphicsRequirementsKHR(system_id, xr_dispatch);

    GLint major = 0;
    GLint minor = 0;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
    auto gl_version = xr::getOpenGLVersion();
    if (gl_version < graphicsRequirements.minApiVersionSupported || gl_version > graphicsRequirements.maxApiVersionSupported) {
        throw std::runtime_error("Runtime does not support desired Graphics API version!");
    }
    return 0;
}

int Engine::InitOpenXRSession(android_app *app)
{
    LOGI("In InitOpenXRSession");
    auto graphicsBinding = xr::GraphicsBindingOpenGLESAndroidKHR{gl_context_->GetDisplay(),
                                                                 //! @todo hello_xr just puts 0 here?
                                                                 gl_context_->GetConfig(), gl_context_->GetContext()};
    auto sessionCreateInfo = xr::SessionCreateInfo{{}, system_id};
    sessionCreateInfo.next = &graphicsBinding;
    session = instance->createSessionUnique(sessionCreateInfo, xr_dispatch);
    local_space = session->createReferenceSpaceUnique(
        xr::ReferenceSpaceCreateInfo{xr::ReferenceSpaceType::Local, xr::Posef{xr::Quaternionf{}, xr::Vector3f{0.f, 0.f, -1.f}}},
        xr_dispatch);

    // This is fairly crude
    std::vector<int64_t> swapchain_formats = session->enumerateSwapchainFormatsToVector(xr_dispatch);
    int64_t preferred_swapchain_format = GL_SRGB8_ALPHA8_EXT;
    int64_t preferred_depth_swapchain_format = GL_DEPTH_COMPONENT32F;

    // just grab the first one
    swapchain_format = swapchain_formats.front();
    depth_swapchain_format = swapchain_formats.front();
    for (int64_t format : swapchain_formats) {
        if (format == preferred_swapchain_format) {
            // hooray!
            swapchain_format = format;
        }
        if (format == preferred_depth_swapchain_format) {
            // hooray!
            depth_swapchain_format = format;
        }
    }

    /* All OpenGL textures that will be submitted in xrEndFrame are created by the runtime here.
     * The runtime will give us a number (not controlled by us) of OpenGL textures per swapchain
     * and tell us with xrAcquireSwapchainImage, which of those we can render to per frame.
     * Here we use one swapchain per view (eye), and for example 3 ("triple buffering") images per
     * swapchain.
     */
    swapchains.resize(viewconfig_views.size());
    projection_views.resize(viewconfig_views.size());

    const auto view_count = viewconfig_views.size();
    for (uint32_t i = 0; i < view_count; i++) {

        SwapchainData &data = swapchains[i];
        data.swapchain = session->createSwapchainUnique(
            xr::SwapchainCreateInfo{xr::SwapchainCreateFlags{},
                                    xr::SwapchainUsageFlagBits::ColorAttachment | xr::SwapchainUsageFlagBits::Sampled, swapchain_format,
                                    viewconfig_views[i].recommendedSwapchainSampleCount, viewconfig_views[i].recommendedImageRectWidth,
                                    viewconfig_views[i].recommendedImageRectHeight, 1, 1, 1},
            xr_dispatch);
        data.images = data.swapchain->enumerateSwapchainImagesToVector<xr::SwapchainImageOpenGLESKHR>(xr_dispatch);
        data.framebuffers.resize(data.images.size());
        glGenFramebuffers(data.images.size(), data.framebuffers.data());
        // We will fill in pose and fov every frame
        projection_views[i] = xr::CompositionLayerProjectionView{
            xr::Posef{}, xr::Fovf{},
            xr::SwapchainSubImage{data.swapchain.get(),
                                  xr::Rect2Di{xr::Offset2Di{0, 0}, xr::Extent2Di{(int32_t)viewconfig_views[i].recommendedImageRectWidth,
                                                                                 (int32_t)viewconfig_views[i].recommendedImageRectHeight}},
                                  0}};
    }
    return 0;
}

void Engine::HandleOpenXREvents()
{
//    LOGI("In HandleOpenXREvents");
    if (!instance) {
        // skip if no instance yet
        return;
    }
    xr::Result result = xr::Result::Success;
    do {
        xr::EventDataBuffer runtime_event;
        result = instance->pollEvent(runtime_event, xr_dispatch);
        if (result == xr::Result::Success) {
            switch (runtime_event.type) {
            case xr::StructureType::EventDataEventsLost: {
                auto *event = reinterpret_cast<xr::EventDataEventsLost *>(&runtime_event);
                (void)event;
                LOGI("EVENT: %d events data lost!\n", event->lostEventCount);
                // do we care if the runtime loses events?
                break;
            }
            case xr::StructureType::EventDataInstanceLossPending: {
                auto *event = reinterpret_cast<xr::EventDataInstanceLossPending *>(&runtime_event);
                (void)event;
                LOGI("EVENT: instance loss pending at %" PRId64 "! Destroying instance.\n", get(event->lossTime));
                session_stopping = true;
                break;
            }
            case xr::StructureType::EventDataSessionStateChanged: {
                auto *event = reinterpret_cast<xr::EventDataSessionStateChanged *>(&runtime_event);
                (void)event;
                // printf("EVENT: session state changed from %d to %d\n", self->state, event->state);
                LOGI("EVENT: session state changed from %s to %s\n", to_string_literal(state), to_string_literal(event->state));
                state = event->state;

                if (event->state >= xr::SessionState::Stopping) {
                    LOGI("Session is stopping...\n");
                    // still handle rest of the events instead of immediately quitting
                    session_stopping = true;
                    session->endSession(xr_dispatch);
                }
                if (event->state == xr::SessionState::Ready) {
                    LOGI("doing beginSession\n");
                    session->beginSession(xr::SessionBeginInfo{view_config_type}, xr_dispatch);
                    running = true;
                }
                break;
            }
            case xr::StructureType::EventDataReferenceSpaceChangePending: {
                LOGI("EVENT: reference space change pending!\n");
                auto *event = reinterpret_cast<xr::EventDataReferenceSpaceChangePending *>(&runtime_event);
                (void)event;
                // TODO: do something
                break;
            }
            case xr::StructureType::EventDataInteractionProfileChanged: {
                LOGI("EVENT: interaction profile changed!\n");
                auto *event = reinterpret_cast<xr::EventDataInteractionProfileChanged *>(&runtime_event);
                (void)event;
#if 0
                    XrInteractionProfileState state = {.type = XR_TYPE_INTERACTION_PROFILE_STATE};

                    for (int i = 0; i < 2; i++) {
                        XrResult res = xrGetCurrentInteractionProfile(self->session, self->hand_paths[i], &state);
                        if (!xr_result(self->instance, res, "Failed to get interaction profile for %d", i))
                            continue;

                        XrPath prof = state.interactionProfile;

                        uint32_t strl;
                        char profile_str[XR_MAX_PATH_LENGTH];
                        res = xrPathToString(self->instance, prof, XR_MAX_PATH_LENGTH, &strl, profile_str);
                        if (!xr_result(self->instance, res, "Failed to get interaction profile path str for %s", h_p_str(i)))
                            continue;

                        printf("Event: Interaction profile changed for %s: %s\n", h_p_str(i), profile_str);
                    }
#endif
                // TODO: do something
                break;
            }

            case xr::StructureType::EventDataVisibilityMaskChangedKHR: {
                LOGI("EVENT: visibility mask changed!!\n");
                auto *event = reinterpret_cast<xr::EventDataVisibilityMaskChangedKHR *>(&runtime_event);
                (void)event;
                // this event is from an extension
                break;
            }
            case xr::StructureType::EventDataPerfSettingsEXT: {
                LOGI("EVENT: perf settings!\n");
                auto *event = reinterpret_cast<xr::EventDataPerfSettingsEXT *>(&runtime_event);
                (void)event;
                // this event is from an extension
                break;
            }
            default:
                LOGI("Unhandled event type %d\n", runtime_event.type);
            }
        }
    } while (result == xr::Result::Success);
}

/**
 * Just the current frame in the display.
 */
void Engine::DrawFrame()
{
    if (!session) {
        LOGI("No session in DrawFrame, skipping out...");
        return;
    }
    HandleOpenXREvents();
    if (session_stopping) {
        UnloadResources();
        return;
    }
    if (!running) {

        // Throttle loop since xrWaitFrame won't be called.
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
        return;
    }
    xr::FrameState frameState = session->waitFrame(xr::FrameWaitInfo{}, xr_dispatch);
    xr::ViewState viewState;
    views = session->locateViewsToVector(
        xr::ViewLocateInfo{view_config_type, frameState.predictedDisplayTime, get(local_space)}, put(viewState), xr_dispatch);
    renderer_.Update(views);

    session->beginFrame(xr::FrameBeginInfo{}, xr_dispatch);

    bool rendered = RenderFrame(frameState, viewState);

    if (rendered) {
        xr::CompositionLayerProjection projectionLayer = {
                {}, local_space.get(), static_cast<uint32_t>(projection_views.size()),
                projection_views.data()};
        xr::CompositionLayerBaseHeader *layers[] = {&projectionLayer};
        xr::FrameEndInfo frameEndInfo = {frameState.predictedDisplayTime,
                                         xr::EnvironmentBlendMode::Opaque, 1, layers};
        session->endFrame(frameEndInfo, xr_dispatch);
    } else {
        xr::FrameEndInfo frameEndInfo = {frameState.predictedDisplayTime,
                                         xr::EnvironmentBlendMode::Opaque, 0, nullptr};
        session->endFrame(frameEndInfo, xr_dispatch);

    }

    // Swap
    //    if (EGL_SUCCESS != gl_context_->Swap()) {
    //        UnloadResources();
    //        LoadResources();
    //    }
}

bool Engine::RenderFrame(const xr::FrameState &frameState, const xr::ViewState &viewState) {
    if (!(viewState.viewStateFlags & xr::ViewStateFlagBits::OrientationValid)) {
        // skip frame
        return false;
    }
    if (frameState.shouldRender == 0) {
        // skip frame
        return false;
    }
    for (size_t i = 0; i < 2; ++i) {
        projection_views[i].pose = views[i].pose;
        projection_views[i].fov = views[i].fov;
        uint32_t acquired_index = swapchains[i].swapchain->acquireSwapchainImage(xr::SwapchainImageAcquireInfo{}, xr_dispatch);
        swapchains[i].swapchain->waitSwapchainImage(xr::SwapchainImageWaitInfo(xr::Duration::infinite()), xr_dispatch);
        glBindFramebuffer(GL_FRAMEBUFFER, swapchains[i].framebuffers[acquired_index]);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, swapchains[i].images[acquired_index].image, 0);
        int w = viewconfig_views[i].recommendedImageRectWidth;
        int h = viewconfig_views[i].recommendedImageRectHeight;
        glViewport(0, 0, w, h);
        glScissor(0, 0, w, h);
        // Just fill the screen with a color.
        glClearColor(0.0f, 0.0f, 0.2f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        renderer_.Render(i);
        glBindVertexArray(0);
        glUseProgram(0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        swapchains[i].swapchain->releaseSwapchainImage(xr::SwapchainImageReleaseInfo(), xr_dispatch);
    }
    return true;
}

/**
 * Tear down the EGL context currently associated with the display.
 */
void Engine::TermDisplay()
{
    LOGI("Suspend context");
    gl_context_->Suspend();
}

void Engine::TrimMemory()
{
    LOGI("Trimming memory");
    gl_context_->Invalidate();
}

/**
 * Process the next input event.
 */
int32_t Engine::HandleInput(android_app *app, AInputEvent *event)
{
    LOGI("HandleInput");
    Engine *eng = (Engine *)app->userData;
    if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) {
        ndk_helper::GESTURE_STATE doubleTapState = eng->doubletap_detector_.Detect(event);
        ndk_helper::GESTURE_STATE dragState = eng->drag_detector_.Detect(event);
        ndk_helper::GESTURE_STATE pinchState = eng->pinch_detector_.Detect(event);

        // Double tap detector has a priority over other detectors
        if (doubleTapState == ndk_helper::GESTURE_STATE_ACTION) {
            // Detect double tap
            eng->tap_camera_.Reset(true);
        }
        else {
            // Handle drag state
            if (dragState & ndk_helper::GESTURE_STATE_START) {
                // Otherwise, start dragging
                ndk_helper::Vec2 v;
                eng->drag_detector_.GetPointer(v);
                eng->TransformPosition(v);
                eng->tap_camera_.BeginDrag(v);
            }
            else if (dragState & ndk_helper::GESTURE_STATE_MOVE) {
                ndk_helper::Vec2 v;
                eng->drag_detector_.GetPointer(v);
                eng->TransformPosition(v);
                eng->tap_camera_.Drag(v);
            }
            else if (dragState & ndk_helper::GESTURE_STATE_END) {
                eng->tap_camera_.EndDrag();
            }

            // Handle pinch state
            if (pinchState & ndk_helper::GESTURE_STATE_START) {
                // Start new pinch
                ndk_helper::Vec2 v1;
                ndk_helper::Vec2 v2;
                eng->pinch_detector_.GetPointers(v1, v2);
                eng->TransformPosition(v1);
                eng->TransformPosition(v2);
                eng->tap_camera_.BeginPinch(v1, v2);
            }
            else if (pinchState & ndk_helper::GESTURE_STATE_MOVE) {
                // Multi touch
                // Start new pinch
                ndk_helper::Vec2 v1;
                ndk_helper::Vec2 v2;
                eng->pinch_detector_.GetPointers(v1, v2);
                eng->TransformPosition(v1);
                eng->TransformPosition(v2);
                eng->tap_camera_.Pinch(v1, v2);
            }
        }
        return 1;
    }
    return 0;
}

/**
 * Process the next main command.
 */
void Engine::HandleCmd(struct android_app *app, int32_t cmd)
{
    Engine *eng = (Engine *)app->userData;
    switch (cmd) {
    case APP_CMD_SAVE_STATE:
        LOGI("APP_CMD_SAVE_STATE");
        break;
    case APP_CMD_INIT_WINDOW:
        LOGI("APP_CMD_INIT_WINDOW");
        // The window is being shown, get it ready.
        if (app->window != NULL) {
            eng->InitDisplay(app);
            eng->has_focus_ = true;
            eng->DrawFrame();
        }
        break;
    case APP_CMD_TERM_WINDOW:
        LOGI("APP_CMD_TERM_WINDOW");
        // The window is being hidden or closed, clean it up.
        eng->TermDisplay();
        eng->has_focus_ = false;
        break;
    case APP_CMD_STOP:
        LOGI("APP_CMD_STOP");
        break;
    case APP_CMD_GAINED_FOCUS:
        LOGI("APP_CMD_GAINED_FOCUS");
        // eng->ResumeSensors();
        // Start animation
        eng->has_focus_ = true;
        break;
    case APP_CMD_LOST_FOCUS:
        LOGI("APP_CMD_LOST_FOCUS");
        eng->SuspendSensors();
        // Also stop animating.
        eng->has_focus_ = false;
        eng->DrawFrame();
        break;
    case APP_CMD_LOW_MEMORY:
        LOGI("APP_CMD_LOW_MEMORY");
        // Free up GL resources
        eng->TrimMemory();
        break;
    }
}

void Engine::SuspendSensors()
{
    LOGI("SuspendSensors");
    // When our app loses focus, we stop monitoring the accelerometer.
    // This is to avoid consuming battery while not being used.
    if (accelerometer_sensor_ != NULL) {
        ASensorEventQueue_disableSensor(sensor_event_queue_, accelerometer_sensor_);
    }
}

//-------------------------------------------------------------------------
// Misc
//-------------------------------------------------------------------------
void Engine::SetState(android_app *state)
{
    LOGI("SetState");
    app_ = state;
    doubletap_detector_.SetConfiguration(app_->config);
    drag_detector_.SetConfiguration(app_->config);
    pinch_detector_.SetConfiguration(app_->config);
}

bool Engine::IsReady() const
{
    return running;
}

void Engine::TransformPosition(ndk_helper::Vec2 &vec)
{
    vec = ndk_helper::Vec2(2.0f, 2.0f) * vec / ndk_helper::Vec2(gl_context_->GetScreenWidth(), gl_context_->GetScreenHeight()) -
          ndk_helper::Vec2(1.f, 1.f);
}

void Engine::ShowUI()
{
    JNIEnv *jni;
    app_->activity->vm->AttachCurrentThread(&jni, NULL);

    // Default class retrieval
    jclass clazz = jni->GetObjectClass(app_->activity->clazz);
    jmethodID methodID = jni->GetMethodID(clazz, "showUI", "()V");
    jni->CallVoidMethod(app_->activity->clazz, methodID);

    app_->activity->vm->DetachCurrentThread();
    return;
}


Engine g_engine;

/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(android_app *state)
{

    g_engine.SetState(state);

    // Init helper functions
    ndk_helper::JNIHelper::Init(state->activity, HELPER_CLASS_NAME);

    state->userData = &g_engine;
    state->onAppCmd = Engine::HandleCmd;
    state->onInputEvent = Engine::HandleInput;

#ifdef USE_NDK_PROFILER
    monstartup("libTeapotNativeActivity.so");
#endif

    // loop waiting for stuff to do.
    while (1) {
        // Read all pending events.
        int id;
        int events;
        android_poll_source *source;

        // If not animating, we will block forever waiting for events.
        // If animating, we loop until all events are read, then continue
        // to draw the next frame of animation.
        while ((id = ALooper_pollAll(g_engine.IsReady() ? 0 : -1, NULL, &events, (void **)&source)) >= 0) {
            // Process this event.
            if (source != NULL)
                source->process(state, source);

            // Check if we are exiting.
            if (state->destroyRequested != 0) {
                if (g_engine.session) {
                    g_engine.session->requestExitSession(g_engine.xr_dispatch);
                }
                g_engine.TermDisplay();
                return;
            }
        }

        // Drawing is throttled to the screen update rate, so there
        // is no need to do timing here.
        g_engine.DrawFrame();
    }
}
